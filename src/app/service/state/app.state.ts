import { EventEmitter, Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root',
})
export class AppState {

    changeLanguageEvents: EventEmitter<void> = new EventEmitter();

}