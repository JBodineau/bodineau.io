import { Component, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AppState } from './service/state/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  constructor(private appState: AppState,
              private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('en');
  }
  
  //The texts to rotate (under my name)
  textsToRotate: string[];
  textRotation: string = '';
  rotationInterval: any;
  rotatedTextVisible: boolean = true;

  //Languages menu
  languageList = [
    { code: 'en', name: 'English' },
    { code: 'fr', name: 'Français' }
  ];
  selectedLanguage = this.languageList[0];
  //The subscription will be useful in the case I choose to add more pages in this app
  langSubscription: Subscription;

  ngOnInit() {
    let lang = localStorage.getItem("LANGUAGE");
    if(!lang) {
      var userLang = navigator.language.split("-")[0];
      this.selectLanguage(userLang, false);
    } else {
      this.selectLanguage(lang, false);
    }
    this.loadRotatedTexts();
    this.langSubscription = this.appState.changeLanguageEvents.subscribe(() => this.loadRotatedTexts());
  }

  ngOnDestroy() {
    if(this.langSubscription) {
      this.langSubscription.unsubscribe();
    }
    if(this.rotationInterval) {
      clearInterval(this.rotationInterval);
    }
  }

  loadRotatedTexts() {
    if(this.rotationInterval) {
      clearInterval(this.rotationInterval);
    }
    this.translate.get("intro_skill_rotate").subscribe(text => {
      this.textsToRotate = text.split(',');
      this.textRotation = this.textsToRotate[0];
      this.rotationInterval = setInterval(()=>this.rotateText(),6000);
    });
  }

  rotateText() {
    let current = this.textsToRotate.indexOf(this.textRotation);
    current += 1;
    if(current >= this.textsToRotate.length) {
      current = 0;
    }
    this.rotatedTextVisible = false;
    setTimeout(() => {
      this.textRotation = this.textsToRotate[current];
      this.rotatedTextVisible = true;
    },2000);
    
  }

  selectLanguage(lang: string, reload: boolean) {
    const selectedLang = this.languageList.filter(language => language.code == lang);
    if(selectedLang.length == 0) {
      this.selectedLanguage = this.languageList[0];
      this.translate.use('en');
    } else {
      this.selectedLanguage = selectedLang[0];
      this.translate.use(lang);
      localStorage.setItem("LANGUAGE", lang);
    }
    this.appState.changeLanguageEvents.emit();
  }
}
