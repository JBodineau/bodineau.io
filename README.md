# bodineau.io website

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.4.

## Prequesites

The [LTS version of node.js](https://nodejs.org/en/download/) must be installed in order to use the npm tools.

Then, install angular-cli globally with npm :
```
npm install -g @angular/cli
```

And install the dependencies :
```
npm install
```

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Packaging - CI

This project is packaged in a Nginx docker container.

When a Git Tag is created, a gitlab-ci pipeline is triggered for the build of the docker image.

This docker image is automatically pushed in a private docker registry.

To install or update the package in the production server, a docker-compose script is used :
```
docker-compose stop
docker-compose pull
docker-compose up -d
```

## Production server proxy

Once started, the application is listening on the 8081 port. So, the nginx proxy of the production server need to redirect the https traffic received for bodineau.io to the 8081 port.